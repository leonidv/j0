package j0;

import java.util.Calendar;
import java.util.Date;

import static com.google.common.base.Preconditions.*;

/**
 * ���������� ������� ���� ��������� ��������� ������� (�� ����������� ���������� �������)
 * 
 * @author l.vygovsky
 * 
 */
public class Person {
	/**
	 * ������������� ���� ������. �������� ��������� ��������:
	 * <ul>
	 * <li>������� ���������� ��������� ���� ����� {@link Person#setSuppose(Person)}
	 * <li>���� ���������� ������
	 * </ul>
	 * 
	 * @param husband
	 * @param wife
	 * @return ������, ���� �������������� �������� � ����������.
	 * @throws MarriageException
	 *             � ������, ���� ���� �� ����������� �� ����� �������� ����
	 */
	public static void marry(Person husband, Person wife)
			throws MarriageException {
		if (husband.isMarried()) {
			throw new MarriageException("husband is married [" + husband + "]");
		}

		if (wife.isMarried()) {
			throw new MarriageException("wife is married [" + husband + "]");
		}

		husband.setSuppose(wife);
		wife.setSuppose(husband);

		int husbandChildrens = husband.getChildrenCount();
		int wifeChildrens = husband.getChildrenCount();
		int totalChildrens = husbandChildrens + wifeChildrens;

		husband.setChildrenCount(totalChildrens);
		wife.setChildrenCount(totalChildrens);
	}

	public static Person birthBaby(Person mother, Person father) {
		Date today = new Date();

		String babyName = mother.getName() + " " + father.getName();
		Person baby = new Person(today, babyName, 0);

		mother.setChildrenCount(mother.getChildrenCount() + 1);
		father.setChildrenCount(father.getChildrenCount() + 1);

		return baby;
	}

	private String name;

	private Person suppose;

	private final Date birthday;

	private int childrenCount;

	/**
	 * ������� ������� � ������ � ���� ��������.
	 * 
	 * @param name
	 * @param birthday
	 */
	public Person(Date birthday, String name, int childCount) {
		this.name = name;
		this.birthday = birthday;
		this.childrenCount = childCount;
	}

	/**
	 * ������� ������� ������ � ���� ��������, ��� ���� � �������� ����� ��������������� ������ ������.
	 * 
	 * @param b
	 */
	public Person(int cc, Date b) {
		this(b, "", cc);
	}

	/**
	 * ��� �������
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * ������(�) �������.
	 * <p>
	 * null ���� ������� �� ������� � �����
	 * 
	 * @return
	 */
	public Person getSuppose() {
		return suppose;
	}

	private void setSuppose(Person suppose) {
		this.suppose = suppose;
	}

	/**
	 * ���� ��������
	 * 
	 * @return
	 */
	public Date getBirthday() {
		return birthday;
	}

	/**
	 * ���������� ������� �������.
	 * 
	 * @return
	 */
	public int getAge() {
		Date today = new Date();
		long ageInMs = today.getTime() - birthday.getTime();
		return new Date(ageInMs).getYear();
	}

	/**
	 * ���������� ���������� �����.
	 * 
	 * @return
	 */
	public int getChildrenCount() {
		return childrenCount;
	}

	public void setChildrenCount(int childrenCount) {
		this.childrenCount = childrenCount;
	}

	/**
	 * ���������, ����� ������� ������(�).
	 * 
	 * @return
	 */
	public boolean isMarried() {
		return suppose != null;
	}

	public boolean isMarriedBy(Person person) {
		return this.isMarried() && this.getSuppose().equals(person);
	}

	/**
	 * ��������� ������� ������� �����. ��� ���� ������ ���������.
	 * 
	 * @param partner
	 * @return ���������� ������, ���� ������� ����� ����� ����� �� ���������� �������.
	 */
	public void makeBaby(Person partner) {
		checkState(this.isMarried() && !this.isMarriedBy(partner),
				"this married with other person (partner = " + partner + ")");
		
		checkArgument(!this.isMarried() && partner.isMarried(),
				"patner married with other person (partner = " + partner + ")");
		// / XXXX �������
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((birthday == null) ? 0 : birthday.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (birthday == null) {
			if (other.birthday != null)
				return false;
		} else if (!birthday.equals(other.birthday))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", suppose.name=" + suppose.name
				+ ", birthday=" + birthday + ", childrenCount=" + childrenCount
				+ "]";
	}

}
