package j0.zoo;

/**
 * ������� �����, ����������� ���������.
 * 
 * @author l.vygovsky
 * 
 */
public abstract class Pet implements Comparable<Pet> {
	private final String name;

	protected int hungerBall = 1;
	
	/**
	 * ������� ��������, �������� ��� ������.
	 * 
	 * @param name
	 */
	public Pet(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	/**
	 * ���������� ����, ������� ������ ��������.
	 * 
	 * @return
	 */
	public abstract String getVoice();

	/**
	 * ���������� �������� ������ � ���������� ��������� ��� ������� �� 10 ������� �����
	 * 
	 * 1 - ��������, 10 ��������� �����.
	 * 
	 * @return
	 */
	public int eat() {
		return 5;
	}

	public int compareTo(Pet o) {
		return name.compareTo(o.getName());
	}

	@Override
	public String toString() {
		return "Pet [name=" + name + "]";
	}
	
	
}
