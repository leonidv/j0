package j0.zoo;

import java.util.Comparator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Zoo {

	public static void main(String[] args) {
		
		Pet a = new Cat("a");
		Pet b = new Dog("B");
		Pet c = new Duck("x");
		
		Set<Pet> set = new TreeSet<Pet>(new Comparator<Pet>() {
			public int compare(Pet o1, Pet o2) {
				
				return o1.getName().compareToIgnoreCase(o2.getName());
			}
		});
		
		set.add(b);
		set.add(c);
		set.add(a);
		
		
		System.out.println(set);
		
		
		Pet pet;
		
		Scanner scanner = new Scanner(System.in);
		if (scanner.next().equals("cat")) {
			pet = new Cat("alice");
		} else {
			pet = new Duck("bob");
		}
		
		System.out.println(pet.getVoice());
		
		scanner.close();
	}

	public static String catMeow() {
		return "meow";
	}
	
	public static String petMeow() {
		return null;
	}
	
	public static void feed(Pet pet) {
		int hungerBall = pet.eat();
		while (hungerBall == 10 ) {
			hungerBall = pet.eat();
		}
	}
}
