package j0.zoo;

public class Duck extends Pet {
	
	public Duck(String name) {
		super(name + name);
	}

	@Override
	public String getVoice() {
		return "quack";
	}

	@Override
	public int eat() {
		hungerBall += 2;
		return hungerBall;
	}
	
	
}
