package j0.zoo;

public interface Hunter {
	
	public abstract void hunt();
}
