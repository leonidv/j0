package j0.zoo;

public class Cat extends Pet implements Friendly {
	
	
	
	public Cat(String name) {
		super(name);
	}
	
	@Override
	public String getVoice() {
		return "meow";
	}

	@Override
	public int eat() {
		hungerBall += 1;
		return hungerBall;
	}
	
	public void scratch() {
		
	}

	public void makeFriendWith() {
		// TODO Auto-generated method stub
		
	}
}
