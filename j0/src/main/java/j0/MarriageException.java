package j0;

public class MarriageException extends Exception {
	private static final long serialVersionUID = 9065769470221922663L;

	public MarriageException() {
		super();
	}

	public MarriageException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MarriageException(String message, Throwable cause) {
		super(message, cause);
	}

	public MarriageException(String message) {
		super(message);
	}

	public MarriageException(Throwable cause) {
		super(cause);
	}
	
}
