package j0;

import java.util.List;

public class JavaFlow {

	/**
	 * ���������� ����������� ����� �� ���� ������������.
	 * <p>
	 * ������������ ��� ������������ ����������� <code>if</code>
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public int min(int x, int y) {
		if (x < y) {
			return x;
		} else {
			return y;
		}
	}

	/**
	 * ���������� �������, ������������ �� ��������� ����������� ������
	 * 
	 * @param data
	 * @return
	 */
	public String mkString(List<Integer> data) {
		String s = "";
		for (int e : data) {
			s = s + e + " ";
		}

		return s;
	}

	/**
	 * ������� ����������� ������� � ���������� ������.
	 * <p>
	 * ������������ ��� ������������ ����� for-each (
	 * 
	 * @param data
	 * @return
	 */
	public int minForEach(List<Integer> data) {
		int min = data.get(0);

		for (int e : data) {
			min = min(min, e);
		}

		return min;
	}

	/**
	 * ������� ����������� ������� � ���������� ������.
	 * <p>
	 * ������������ ��� ������������ ����� for
	 * <p>
	 * ����� ����������� ����������, ��� {@link #minForEach(List)}.
	 * 
	 * @param data
	 * @return
	 */
	public int minForIndex(List<Integer> data) {
		int min = data.get(0);
		
		for (int i = 1; i < data.size(); i++) {
			int e = data.get(i);
			min = min(min,e);
		}
		
		return min;
	}
	
	/**
	 * ���������, ��������� �� ������� � ��������� ������.
	 * <p>
	 * ������������ ��� ������������ ����� while 
	 * @param data
	 * @param x
	 * @return
	 */
	public boolean containsWhile(List<Integer> data, int x) {
		boolean found = false;
		
		int index = 0;
		int size = data.size();
		
		while (index < size && !found) {
			int e = data.get(index);
			found = (e == x);
			index += 1;
		}
		
		return found;
	}
	
	/**
	 * ���������, ��������� �� ������� � ��������� ������.
	 * <p>
	 * ������������ ��� ������������ ������� ����� ����������� ������ 
	 * @param data
	 * @param x
	 * @return
	 */	
	public boolean containsForEach(List<Integer> data, int x) {
		for (int e : data) {
			if (e == x) {
				return true;
			}
		}
		
		return false;
	}
}
