public class ExceptionRun {

	public static void main(String[] args) {
		try {
			foo();
		} catch (IllegalStateException e) {
			e.printStackTrace(System.out);
		} catch (RuntimeException e) {
			System.out.println(":-O");
			e.printStackTrace(System.out);
		} finally {
			System.out.println(":)");
		}
		
		System.out.println(":) :)");
	}

	public static void foo() {
		bar();
		bar();
	}

	private static void bar() {
		if (1 == 1)
			throw new IllegalStateException();
		buzz();
	}

	private static void buzz() {
		throw new RuntimeException("Hello from buzz");

	}
}
