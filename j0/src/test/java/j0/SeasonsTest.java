package j0;

import static org.junit.Assert.*;

import org.junit.Test;

public class SeasonsTest {
	
	@Test
	public void shouldDetectWinter() {
		Seasons season = new Seasons();
		String actual = season.getSeason(1);
		
		assertEquals("winter", actual);
	}
	
	@Test
	public void shouldDetectSpring() {
		Seasons season = new Seasons();
		String actual = season.getSeason(4);
		
		assertEquals("spring", actual);
	}

	@Test
	public void shouldDetectSummer() {
		Seasons season = new Seasons();
		String actual = season.getSeason(7);
		assertEquals("summer", actual);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionOnBadMonth() {
		Seasons season = new Seasons();
		season.getSeason(100);
	}
	
}
