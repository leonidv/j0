package j0;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import static org.junit.Assert.*;

public class JavaFlowTest {
	
	
	@Test
	public void shouldSelectMinimalFromTwoElements() {
		JavaFlow javaFlow = new JavaFlow();
		int actual = javaFlow.min(0, 1);
		assertEquals(0, actual);
	}
	
	@Test
	public void shouldMakeStringWithElements() {
		JavaFlow javaFlow = new JavaFlow();
		
		List<Integer> data = new ArrayList<Integer>();
		data.add(0);
		data.add(1);
		
		String actual = javaFlow.mkString(data);
		
		assertEquals("0 1 ", actual);
	}
	
	@Test
	public void shouldFindMinimalWithForEach() {
		JavaFlow javaFlow = new JavaFlow();
		
		List<Integer> data = new ArrayList<Integer>();
		data.add(0);
		data.add(1);
		
		int actual = javaFlow.minForEach(data);
		
		assertEquals(0, actual);
	}
	
	@Test
	public void shouldFindMinimalWithForIndex() {
		JavaFlow javaFlow = new JavaFlow();
		
		List<Integer> data = new ArrayList<Integer>();
		data.add(0);
		data.add(1);
		
		int actual = javaFlow.minForIndex(data);
		
		assertEquals(0, actual);
	}	
	
	@Test
	public void shouldDetectContainsWhile() {
		JavaFlow javaFlow = new JavaFlow();
		
		List<Integer> data = new ArrayList<Integer>();
		data.add(0);
		data.add(1);
		
		boolean actual = javaFlow.containsWhile(data, 0);
		
		assertEquals(true, actual);
	}	

	@Test
	public void shouldDetectContainsForEach() {
		JavaFlow javaFlow = new JavaFlow();
		
		List<Integer> data = new ArrayList<Integer>();
		data.add(0);
		data.add(1);
		
		boolean actual = javaFlow.containsForEach(data, 0);
		
		assertEquals(true, actual);
	}
}
