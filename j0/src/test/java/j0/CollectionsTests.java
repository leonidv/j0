package j0;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

import static org.junit.Assert.*;
import static com.google.common.collect.Lists.*;
import static com.google.common.collect.Maps.*;
import static com.google.common.collect.Sets.*;

public class CollectionsTests {

	@Test
	public void listIntegerExamples() {
		List<Integer> list = new ArrayList<Integer>();
		list.add(1);
		list.add(2);

		assertTrue(list.contains(1));
		assertFalse(list.contains(3));

		List<Integer> otherList = newArrayList(2, 3);
		list.addAll(otherList);

		assertEquals(4, list.size());

		List<Integer> bothList = newArrayList(1, 2, 2, 3);

		assertEquals(bothList, list);
	}

	@Test
	public void listRemoveExamples() {
		List<String> strings = newArrayList("s1", "s2");

		assertTrue(strings.contains("s1"));
		strings.remove("s1");
		assertFalse(strings.contains("s1"));

		assertTrue(strings.contains("s2"));
		strings.remove(0);
		assertFalse(strings.contains("s2"));
	}

	@Test
	public void listRemoveIntegerExample() {
		List<Integer> data = newArrayList(0, 3, 2, 1, 0);

		data.remove(0);
		assertEquals(newArrayList(3, 2, 1, 0), data);

		data.remove(new Integer(0));
		assertEquals(newArrayList(3, 2, 1), data);
	}

	@Test
	public void setsExample() {
		Set<Integer> set = newHashSet(1, 2);

		assertTrue(set.contains(1));
		assertFalse(set.contains(3));

		Set<Integer> other = newHashSet(2, 3);

		set.addAll(other);

		assertEquals(3, set.size());
		assertEquals(newHashSet(1,2,3),set);
	}

	@Test
	public void mapsExample() {
		Map<Integer, String> map = newHashMap();
		map.put(1, "a");
		map.put(2, "b");
		map.put(3, "c");

		assertTrue(map.containsKey(1));
		assertTrue(map.containsValue("a"));

		assertEquals(newHashSet(1, 2, 3), map.keySet());

		assertEquals(newHashSet("a", "b", "c"), map.values());
	}
}
