package j0;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class JUnitLifecycleDemo {
	
	@BeforeClass
	public static void runBeforeAllTests() {
		System.out.println("before all tests");
	}
	
	@Before
	public void runBeforEachTest() {
		System.out.println("  before each test");
	}
	
	@Test
	public void test1() {
		System.out.println("    test 1");
	}
	
	@Test
	public void test2() {
		System.out.println("    test 2");
	}
	
	@After
	public void runAfterEachTest() {
		System.out.println("  after each test");
	}
	
	@AfterClass
	public static void runAfterAllTests() {
		System.out.println("after all tests");
	}
}
