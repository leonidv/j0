package j0;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import static org.junit.Assert.*;

public class RegexTests {
	
	@Test
	public void stringMatches() {
		assertTrue("abc".matches("abc"));
		assertFalse("abc".matches("123"));
	}
	
	@Test
	public void shouldMatchesPatter() {
		Pattern pattern = Pattern.compile("abc");
		Matcher matcher = pattern.matcher("abc");
		assertTrue(matcher.matches());
		
		matcher = pattern.matcher("123");
		assertFalse(matcher.matches());
		
	}

	@Test
	public void shouldFindGroup() {
		Pattern pattern = Pattern.compile("([�-��-�]+\\s)([�-��-�]+\\s)(\\d{2,4})");
		Matcher matcher = pattern.matcher("������ ������ 1978");
		
		
		
		assertTrue(matcher.find());
		
		assertEquals("������ ", matcher.group(1));
		assertEquals("1978", matcher.group(3));
		
		String s = "������ ������ 1978";
		s = s.split("\\s")[2];
	
	}
}
